<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\SiginController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[IndexController::class,'awal']);

Route::get('/awal',[IndexController::class,'awal']);
Route::get('/form',[IndexController::class,'halaman']);
Route::get('/tabel',[IndexController::class,'tabel']);
Route::post('/home',[SiginController::class,'log']);
Route::resource('/casts', \App\Http\Controllers\CastController::class);