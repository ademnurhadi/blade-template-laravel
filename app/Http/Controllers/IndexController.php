<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function halaman(){
        return view('form');
    }
    
    public function awal() {
        return view ('depan');
    }

    public function tabel() {
        return view ('tabel');
    }

}