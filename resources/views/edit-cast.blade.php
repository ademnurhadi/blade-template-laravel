@extends('template.layout')

@section('content')

<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">

            <div class="content-header">
                <div class="container-fluid">

                    <div class="card-body">
                        <form action="/casts/{{ ( $cast->id) }}" method="POST">
                            @csrf
                            @method('PUT')


                            <div class="form-group">
                                <label class="font-weight-bold">Nama</label>
                                <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama"
                                    value="{{ old('nama', $cast->nama) }}" placeholder="Masukkan nama cast">

                                @error('nama')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label class="font-weight-bold">Umur</label>
                                <input type="number" class="form-control @error('umur') is-invalid @enderror"
                                    name="umur" value="{{ old('umur', $cast->umur) }}" placeholder="Masukkan umur ">

                                @error('umur')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="font-weight-bold">Bio</label>
                                <input type="text" class="form-control @error('bio') is-invalid @enderror" name="bio"
                                    value="{{ old('bio', $cast->bio) }}" placeholder="Masukkan bio ">

                                @error('bio')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>



                            <button type="submit" class="btn btn-md btn-primary">UPDATE</button>
                            <button type="reset" class="btn btn-md btn-warning">RESET</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection