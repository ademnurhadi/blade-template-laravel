

@extends('template.layout')

@section('content')

<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">

            <div class="content-header">
                <div class="container-fluid">

                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-2">
                                <div class="col">

                                   <div class="card mt-5 bg-info ">
                                    <div class="card-body">
                                        <h3 class="text-center border-bottom">Selamat Anda Berhasil Daftar</h3>
                                        <h4>Selamat datang {{ $namadepan }} {{ $namabelakang }}</h4>
                                        <h5>Mari Berbelanja Dengan website kami!!!</h5>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection