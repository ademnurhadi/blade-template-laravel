@extends('template.layout')

@section('content')

<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">

            <div class="content-header">
                <div class="container-fluid">

                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-2">
                                <div class="col-sm-6">

                                    <h3>Halaman Home</h3>
                                    <h1> Halaman Toko Online Tamvan </h1>

                                    <p>Bahagia berbelanja bersama kami</p>
                                    <div class="container">
                                        <h1>Keuntungan </h1>

                                        <ul class="list-group">
                                            <li class="list-group-item">1. Murah</li>
                                            <li class="list-group-item">2. Aman</li>
                                            <li class="list-group-item">3. Terpercaya</li>
                                            <li class="list-group-item">4. Luas</li>
                                        </ul>
                                    </div>
                                    <h6>Silahkan klik untuk daftar account ini <a href="/tugas15/public/form"> Form</a></h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection