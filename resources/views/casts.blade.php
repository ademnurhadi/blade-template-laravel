@extends('template.layout')

@section('content')

<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">

            <div class="content-header">
                <div class="container-fluid">

                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-2">
                                <div class="col">

                                    <div class="card mt-5 bg-info ">
                                        <div class="card-body">
                                            <div class="row mb-2">
                                                <div class="col">
                                                    <a href="{{ route('casts.create') }}"
                                                        class="btn btn-md btn-success mb-3">TAMBAH CAST</a>

                                                    <table id="example1" class="table table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Nama</th>
                                                                <th>Umur</th>
                                                                <th>Bio</th>
                                                                <th>Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            @forelse ($cast as $casts)
                                                            <tr>
                                                                <td>{{ $casts->nama }}</td>
                                                                <td>{{ $casts->umur }}</td>
                                                                <td>{{ $casts->bio }}</td>
                                                                <td class="text-center">
                                                                    <form
                                                                        onsubmit="return confirm('Apakah Anda Yakin ?');"
                                                                        action="{{ route('casts.destroy', $casts->id) }}"
                                                                        method="POST">
                                                                        <a href="{{ route('casts.edit', $casts->id) }}"
                                                                            class="btn btn-sm btn-primary">EDIT</a>
                                                                        @csrf
                                                                        @method('DELETE')
                                                                        <button type="submit"
                                                                            class="btn btn-sm btn-danger">HAPUS</button>
                                                                    </form>
                                                                </td>
                                                            </tr>
                                                            @empty
                                                            <div class="alert alert-danger">
                                                                Data Post belum Tersedia.
                                                            </div>
                                                            @endforelse
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection