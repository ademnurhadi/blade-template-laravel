@extends('template.layout')

@section('content')

<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">

            <div class="content-header">
                <div class="container-fluid">

                    <div class="card">
                        <form action="/casts" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="nama">Nama </label>
                                <input type="text" class="form-control" name="nama" id="nama"
                                    placeholder="Masukkan Nama">
                                @error('nama')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="umur">Umur</label>
                                <input type="number" class="form-control" name="umur" id="umur"
                                    placeholder="Masukkan Umur">
                                @error('umur')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="bio">Bio</label>
                                <input type="text" class="form-control" name="bio" id="bio" placeholder="Masukkan bio">
                                @error('bio')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection